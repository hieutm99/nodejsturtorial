// Import events module  
const events = require('events');  
// Tạo một đối tượng EventEmitter mới
const eventEmitter = new events.EventEmitter();  
   
// Tạo hàm để nhận sự kiện
const connectHandler = function connected() {  
   console.log('Kết nối sự kiện connection.');  
     
   // Thực thi sự kiện có tên data_received  
   eventEmitter.emit('data_received');  
}  
   
// Thêm sự kiện connection và khai báo hàm nhận sự kiện
eventEmitter.on('connection', connectHandler);  
// Thêm sự kiện data_received và khai báo hàm nhận sự kiện
eventEmitter.on('data_received', function(){  
   console.log('Kết nối sự kiện data_received.');  
});  
   // Thực thi sự kiện có tên connection  
eventEmitter.emit('connection');