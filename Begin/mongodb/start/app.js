var mongoose = require('mongoose');
mongoose.connect('mongodb+srv://hieutm1999:hieuvd123@cluster0.suhch.mongodb.net/test', {useNewUrlParser: true, useNewUrlParser: true,  useUnifiedTopology: true});

var db = mongoose.connection;
//Bắt sự kiện error
db.on('error', function(err) {
  if (err) console.log(err)
});
//Bắt sự kiện open
db.once('open', function() {
  //Khởi tạo Schema
  var Schema = mongoose.Schema;
  var blogSchema = new Schema({
    title:  String, // String is shorthand for {type: String}
    author: String,
    body:   String,
    hidden: Boolean
  });
  blogSchema.methods.callTitle = function() {
    console.log(`Đã thêm bài viết mới có tên ${this.title}`)
  }
  var Blog = mongoose.model('Blog', blogSchema);
 
  var dataInsert = {
    title:  'Lập trình NodeJS căn bản', 
    author: 'Freetuts.net',
    body:   'Nội dung lập trình NodeJS căn bản',
    hidden: false,
  }
  var blogCollections = new Blog(dataInsert);
  blogCollections.save(function (err, data) {
    if (err) return console.error(err);
    console.log(data)
    blogCollections.callTitle()
  });
});