var mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://hieutm1999:hieuvd123@cluster0.suhch.mongodb.net/test",
  { useNewUrlParser: true, useNewUrlParser: true, useUnifiedTopology: true }
);

var db = mongoose.connection;
//Bắt sự kiện error
db.on("error", function (err) {
  if (err) console.log(err);
});
//Bắt sự kiện open
db.once("open", function () {
  var childSchema = new mongoose.Schema({ name: "string" });

  childSchema.pre("validate", function (next) {
    console.log("childSchema validate.");
    next();
  });

  childSchema.pre("save", function (next) {
    console.log("childSchema save.");
    next();
  });

  var parentSchema = new mongoose.Schema({
    child: [childSchema],
  });

  parentSchema.pre("validate", function (next) {
    console.log("parentSchema validate");
    next();
  });

  parentSchema.pre("save", function (next) {
    console.log("parentSchema save");
    next();
  });
  var Parent = mongoose.model("Parent", parentSchema);
  var parent = new Parent({
    child: [{ name: "Freetuts.net" }, { name: "Lap trinh NodeJS" }],
  });
  parent.save();
});
