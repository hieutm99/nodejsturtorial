var mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://hieutm1999:hieuvd123@cluster0.suhch.mongodb.net/test",
  { useNewUrlParser: true, useNewUrlParser: true, useUnifiedTopology: true }
);

var db = mongoose.connection;
//Bắt sự kiện error
db.on("error", function (err) {
  if (err) console.log(err);
});
//Bắt sự kiện open
db.once("open", function () {
  var Schema = mongoose.Schema;
  var breakfastSchema = new Schema({
    eggs: {
      type: Number,
      min: [1, "Phải ăn ít nhất 1 quả trứng nha !!"],
      max: 12,
    },
    meat: {
      type: Number,
      required: [true, "Phải ăn thịt nữa !!!"],
    },
    drink: {
      type: String,
      enum: ["Coffee", "Tea"],
      required: function () {
        return this.meat > 3;
      },
    },
  });
  var Breakfast = db.model("Breakfast", breakfastSchema);

  var badBreakfast = new Breakfast({
    eggs: 0,
    meat: 0,
    drink: "Milk",
  });
  badBreakfast.validateSync();
});
