//Khai báo thư viện FS của NodeJS
const fs = require('fs') 
//Callback Function
const callbackReadFile = function (err, filedata) {   
    if (err) return console.error(err);   
    console.log(filedata.toString());   
}
//Đọc file bằng hàm readFile
fs.readFile('fileDemo.txt', callbackReadFile);  
 
console.log('Kết thúc chương trình')