var buffer1 = new Buffer('Buffer'); //Tạo một buffer có giá trị là "Buffer"
var buffer2 = new Buffer('is very simple'); //Tạo một buffer có giá trị là "is very simple"
var buffer3 = Buffer.concat([buffer1,buffer2]); //Gộp 2 buffer mới tạo vào với nhau thành buffer3
 
console.log("buffer3 content: " + buffer3.toString()); //In ra giá trị của Buffer mới